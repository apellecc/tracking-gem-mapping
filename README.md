# Tracking GEM mapping

The goal is documenting and storing the readout mapping for 256-strips 2D triple GEM detectors used for tracking.

The APV mapping requires this kind of header:

```
# panasonic pin  # multilayer # layer # readout # strip
```

The multilayer and layer in our case are always 0 and readout must be `X` or `Y`.

In the VFAT mapping, the VFAT number is 0 and 1 for X, 2 and 3 for Y.

## How to run in case of changes

```python
python3 create_apv_mapping.py apv --verbose
```
