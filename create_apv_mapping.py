import argparse
import os
import pathlib
import pandas as pd

connector_map = {
    0: "X",
    1: "X",
    2: "Y",
    3: "Y" 
}

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("odir", type=pathlib.Path)
    parser.add_argument("-v", "--verbose", action="store_true")
    args = parser.parse_args()

    os.makedirs(args.odir, exist_ok=True)

    # Read input VFAT mapping:
    vfat_channel_strip_mapping = pd.read_csv("vfat/channel_to_strip.csv")
    vfat_pin_channel_mapping = pd.read_csv("vfat/pin_to_channel.csv")
    if args.verbose:
        print("VFAT channel to strip mapping:")
        print(vfat_channel_strip_mapping)
        print("VFAT panasonic pin to channel mapping:")
        print(vfat_pin_channel_mapping)

    # Merge the two to get a mapping from strip to pin of Panasonic connector:
    strip_pin_mapping = pd.merge(vfat_channel_strip_mapping, vfat_pin_channel_mapping, how="inner", on="channel")[["vfat", "strip", "pin"]]
    if args.verbose:
        print("Strip to Panasonic pin mapping:")
        print(strip_pin_mapping)

    """ Save in the format required by mpgd-analysis: """
    # since X and Y mappingd are the same, save only half of it:
    reco_mapping = strip_pin_mapping[strip_pin_mapping.vfat<=1]
    reco_mapping.rename(columns={"vfat": "chip"}, inplace=True)
    reco_mapping = reco_mapping[["chip", "pin", "strip"]]
    if args.verbose:
        print("Mapping in the mpgd-analysis format:")
        print(reco_mapping)
    csv_name = args.odir / "gem.csv"
    reco_mapping.to_csv(csv_name, sep=",", index=False)
    print("Saved mapping in mpgd-analysis format to output file {}".format(csv_name))

    # Associate VFATs 0 and 1 to X, 1 and 2 to Y:
    vfat_group = strip_pin_mapping.groupby("vfat")
    for vfat,df in vfat_group:
        df["multilayer"] = 0
        df["layer"] = 0
        df["readout"] = df.vfat.map(connector_map)
        df = df[["pin", "multilayer", "layer", "readout", "strip"]]
        if args.verbose:
            print("Mapping for VFAT {}:".format(vfat))
            print(df)
        csv_name = args.odir / "TrackingGEM256-C{}-map.txt".format(vfat)
        df.to_csv(
            csv_name,
            header=["# panasonic pin", "# multilayer", "# layer", "# readout","# strip"],
            sep="\t", index=False
        )
        print("Saved mapping for chip {} to output file to {}.".format(vfat, csv_name))

if __name__=="__main__":
    main()
